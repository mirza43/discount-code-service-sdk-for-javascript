Feature: Get Discount Code
  Get  discount code synopsis view object with code value

  Scenario: Success
    Given I provide a valid accessToken
    When I execute getDiscountCode
    Then discount code synopsis view object in the discount-code-service is returned