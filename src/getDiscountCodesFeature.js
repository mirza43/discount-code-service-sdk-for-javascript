import {inject} from 'aurelia-dependency-injection';
import DiscountCodeServiceSdkConfig from './discountCodeServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import DiscountCodeSynopsisView from './discountCodeSynopsisView';

@inject(DiscountCodeServiceSdkConfig, HttpClient)
class GetDiscountCodesFeature {

    _config:DiscountCodeServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:DiscountCodeServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists all customer sources
     * @param {string} accessToken
     * @returns {Promise.<DiscountCodeSynopsisView>}
     */
    execute(code:string,
        accessToken:string):Promise<DiscountCodeSynopsisView> {

        return this._httpClient
            .createRequest(`discount-codes/${code}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => response.content);
    }
}

export default GetDiscountCodesFeature;

