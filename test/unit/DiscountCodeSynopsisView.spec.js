import DiscountCodeSynopsisView from '../../src/discountCodeSynopsisView';
import dummy from '../dummy';

describe('DiscountCodeSynopsisView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new DiscountCodeSynopsisView(
                    null,
                    dummy.code,
                    dummy.type,
                    dummy.value,
                    dummy.maxValue,
                    dummy.description,
                    dummy.expirationDate
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new DiscountCodeSynopsisView(
                    expectedId,
                    dummy.code,
                    dummy.type,
                    dummy.value,
                    dummy.maxValue,
                    dummy.description,
                    dummy.expirationDate
                );

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if code is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new DiscountCodeSynopsisView(
                    dummy.discountCodeId,
                    null,
                    dummy.type,
                    dummy.value,
                    dummy.maxValue,
                    dummy.description,
                    dummy.expirationDate
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'code required');
        });

        it('sets code', () => {
            /*
             arrange
             */
            const expectedCode = dummy.code;

            /*
             act
             */
            const objectUnderTest =
                new DiscountCodeSynopsisView(
                    dummy.discountCodeId,
                    expectedCode,
                    dummy.type,
                    dummy.value,
                    dummy.maxValue,
                    dummy.description,
                    dummy.expirationDate
                );

            /*
             assert
             */
            const actualCode = objectUnderTest.code;
            expect(actualCode).toEqual(expectedCode);

        });

        it('throws if type is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new DiscountCodeSynopsisView(
                    dummy.discountCodeId,
                    dummy.code,
                    null,
                    dummy.value,
                    dummy.maxValue,
                    dummy.description,
                    dummy.expirationDate
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'type required');
        });

        it('sets type', () => {
            /*
             arrange
             */
            const expectedType = dummy.type;

            /*
             act
             */
            const objectUnderTest =
                new DiscountCodeSynopsisView(
                    dummy.discountCodeId,
                    dummy.code,
                    expectedType,
                    dummy.value,
                    dummy.maxValue,
                    dummy.description,
                    dummy.expirationDate
                );

            /*
             assert
             */
            const actualType = objectUnderTest.type;
            expect(actualType).toEqual(expectedType);

        });

        it('throws if value is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new DiscountCodeSynopsisView(
                    dummy.discountCodeId,
                    dummy.code,
                    dummy.type,
                    null,
                    dummy.maxValue,
                    dummy.description,
                    dummy.expirationDate
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'value required');
        });

        it('sets value', () => {
            /*
             arrange
             */
            const expectedValue = dummy.value;

            /*
             act
             */
            const objectUnderTest =
                new DiscountCodeSynopsisView(
                    dummy.discountCodeId,
                    dummy.code,
                    dummy.type,
                    expectedValue,
                    dummy.maxValue,
                    dummy.description,
                    dummy.expirationDate
                );

            /*
             assert
             */
            const actualValue = objectUnderTest.value;
            expect(actualValue).toEqual(expectedValue);

        });

        it('throws if description is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new DiscountCodeSynopsisView(
                    dummy.discountCodeId,
                    dummy.code,
                    dummy.type,
                    dummy.value,
                    dummy.maxValue,
                    null,
                    dummy.expirationDate
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'description required');
        });

        it('sets description', () => {
            /*
             arrange
             */
            const expectedDescription = dummy.description;

            /*
             act
             */
            const objectUnderTest =
                new DiscountCodeSynopsisView(
                    dummy.discountCodeId,
                    dummy.code,
                    dummy.type,
                    dummy.value,
                    dummy.maxValue,
                    expectedDescription,
                    dummy.expirationDate
                );

            /*
             assert
             */
            const actualDescription = objectUnderTest.description;
            expect(actualDescription).toEqual(expectedDescription);

        });

        it('throws if expirationDate is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new DiscountCodeSynopsisView(
                    dummy.discountCodeId,
                    dummy.code,
                    dummy.type,
                    dummy.value,
                    dummy.maxValue,
                    dummy.description,
                    null
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'expirationDate required');
        });

        it('sets expirationDate', () => {
            /*
             arrange
             */
            const expectedExpirationDate = dummy.expirationDate;

            /*
             act
             */
            const objectUnderTest =
                new DiscountCodeSynopsisView(
                    dummy.discountCodeId,
                    dummy.code,
                    dummy.type,
                    dummy.value,
                    dummy.maxValue,
                    dummy.description,
                    expectedExpirationDate
                );

            /*
             assert
             */
            const actualExpirationDate = objectUnderTest.expirationDate;
            expect(actualExpirationDate).toEqual(expectedExpirationDate);

        });

    })
});

