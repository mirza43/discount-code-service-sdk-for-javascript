/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    firstName: 'firstName',
    lastName: 'lastName',
    discountCodeId:1,
    accountId:'000000000000000000',
    sapVendorNumber:'0000000000',
    userId:'email@test.com',
    url:'https://dummy-url.com',
    code:'G553QT',
    type:'$',
    value:100,
    maxValue:null,
    description:'Issued to Sam',
    expirationDate:'12/31/2016'
};